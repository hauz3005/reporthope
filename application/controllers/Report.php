<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model("Niños_model","ModelNiños");
        $this->load->model("Medicion_model","ModelMedicion");
        $this->load->model("Bimestre_model","ModelBimestre");
        $this->load->helper('url');
		$this->load->helper('date');
		        $this->load->helper('form');

    }
	public function index()
	{
		$ninasList = $this->ModelNiños->getBySexo('MUJER');
		$ninosList = $this->ModelNiños->getBySexo('HOMBRE');

		// Numero de niños y asistencia

		$ninosMenCount = 0;
		$ninosMayCount = 0;
		$ninasMenCount = 0;
		$ninasMayCount = 0;
		$año = intval(date('Y'));
		foreach ($ninosList as $valor) {
			if( ($año - intval($valor->fechaNacimiento)) < 5 ){
				$ninosMenCount++;
			}
			else{
				$ninosMayCount++;
			}
		}
		foreach ($ninasList as $valor) {

			if( ($año - intval($valor->fechaNacimiento)) < 5 ){
				$ninasMenCount++;
			}
			else{
				$ninasMayCount++;
			}
		}
		
		$datapage['ninosMenCount'] =  $ninosMenCount;
		$datapage['ninosMayCount'] =  $ninosMayCount;
		$datapage['ninasMenCount'] =  $ninasMenCount;
		$datapage['ninasMayCount'] =  $ninasMayCount;




		// Reporte de los ultimos 3 Bimestres

		$bimestresList = $this->ModelBimestre->getByUltimos();
		$bimestres = '';
		
		$dataHomImcMenD = '';
		$dataHomImcMayD = '';
		$dataHomTPMenD = '';
		$dataHomTPMayD = ''; 
		$dataHomImcMenN = '';
		$dataHomImcMayN = '';
		$dataHomTPMenN = '';
		$dataHomTPMayN = ''; 
		$dataHomImcMenS = '';
		$dataHomImcMayS = '';
		$dataHomTPMenS = '';
		$dataHomTPMayS = ''; 

		$dataMujImcMenD = '';
		$dataMujImcMayD = '';
		$dataMujTPMenD = '';
		$dataMujTPMayD = ''; 
		$dataMujImcMenN = '';
		$dataMujImcMayN = '';
		$dataMujTPMenN = '';
		$dataMujTPMayN = ''; 
		$dataMujImcMenS = '';
		$dataMujImcMayS = '';
		$dataMujTPMenS = '';
		$dataMujTPMayS = ''; 

		$auxcont=0;
		$limit=count($bimestresList) -1;
		foreach ($bimestresList as $valor) {
			if($auxcont < $limit){
				$bimestres .='"'.$valor->nombre.'",';
			}
			else{
				$bimestres .='"'.$valor->nombre.'"';
			}

			if($auxcont < $limit){

				$dataHomImcMenD .=$valor->hombresMenorImcD.',';
				$dataHomImcMenN .=$valor->hombresMenorImcN.',';
				$dataHomImcMenS .=$valor->hombresMenorImcS.',';

				$dataHomImcMayD .=$valor->hombresImcD.',';
				$dataHomImcMayN .=$valor->hombresImcN.',';
				$dataHomImcMayS .=$valor->hombresImcS.',';

				$dataHomTPMenD .=$valor->hombresMenorTallaPesoD.',';
				$dataHomTPMenN .=$valor->hombresMenorTallaPesoN.',';
				$dataHomTPMenS .=$valor->hombresMenorTallaPesoS.',';

				$dataHomTPMayD .=$valor->hombresTallaPesoD.',';
				$dataHomTPMayN .=$valor->hombresTallaPesoN.',';
				$dataHomTPMayS .=$valor->hombresTallaPesoS.',';

				$dataMujImcMenD .=$valor->mujeresMenorImcD.',';
				$dataMujImcMenN .=$valor->mujeresMenorImcN.',';
				$dataMujImcMenS .=$valor->mujeresMenorImcS.',';

				$dataMujImcMayD .=$valor->mujeresImcD.',';
				$dataMujImcMayN .=$valor->mujeresImcN.',';
				$dataMujImcMayS .=$valor->mujeresImcS.',';

				$dataMujTPMenD .=$valor->mujeresMenorTallaPesoD.',';
				$dataMujTPMenN .=$valor->mujeresMenorTallaPesoN.',';
				$dataMujTPMenS .=$valor->mujeresMenorTallaPesoS.',';

				$dataMujTPMayD .=$valor->mujeresTallaPesoD.',';
				$dataMujTPMayN .=$valor->mujeresTallaPesoN.',';
				$dataMujTPMayS .=$valor->mujeresTallaPesoS.',';

			}
			else{
				$asistenciaMenHom = $valor->hombresMenorImcD + $valor->hombresMenorImcN + $valor->hombresMenorImcS;
				$asistenciaMayHom = $valor->hombresImcD + $valor->hombresImcN + $valor->hombresImcS;
				$asistenciaMenMuj = $valor->mujeresMenorImcD + $valor->mujeresMenorImcN + $valor->mujeresMenorImcS;
				$asistenciaMayMuj = $valor->mujeresImcD + $valor->mujeresImcN + $valor->mujeresImcS;

				$dataHomImcMenD .=$valor->hombresMenorImcD;
				$dataHomImcMenN .=$valor->hombresMenorImcN;
				$dataHomImcMenS .=$valor->hombresMenorImcS;

				$dataHomImcMayD .=$valor->hombresImcD;
				$dataHomImcMayN .=$valor->hombresImcN;
				$dataHomImcMayS .=$valor->hombresImcS;

				$dataHomTPMenD .=$valor->hombresMenorTallaPesoD;
				$dataHomTPMenN .=$valor->hombresMenorTallaPesoN;
				$dataHomTPMenS .=$valor->hombresMenorTallaPesoS;

				$dataHomTPMayD .=$valor->hombresTallaPesoD;
				$dataHomTPMayN .=$valor->hombresTallaPesoN;
				$dataHomTPMayS .=$valor->hombresTallaPesoS;

				$dataMujImcMenD .=$valor->mujeresMenorImcD;
				$dataMujImcMenN .=$valor->mujeresMenorImcN;
				$dataMujImcMenS .=$valor->mujeresMenorImcS;

				$dataMujImcMayD .=$valor->mujeresImcD;
				$dataMujImcMayN .=$valor->mujeresImcN;
				$dataMujImcMayS .=$valor->mujeresImcS;

				$dataMujTPMenD .=$valor->mujeresMenorTallaPesoD;
				$dataMujTPMenN .=$valor->mujeresMenorTallaPesoN;
				$dataMujTPMenS .=$valor->mujeresMenorTallaPesoS;

				$dataMujTPMayD .=$valor->mujeresTallaPesoD;
				$dataMujTPMayN .=$valor->mujeresTallaPesoN;
				$dataMujTPMayS .=$valor->mujeresTallaPesoS;

			}




		}
		$datapage['bimestresList'] =  $bimestresList;
		$datapage['bimestres'] =  $bimestres;
		$datapage['dataHomImcMenD'] = $dataHomImcMenD;
		$datapage['dataHomImcMenN'] = $dataHomImcMenN;
		$datapage['dataHomImcMenS'] = $dataHomImcMenS; 

		$datapage['dataHomImcMayD'] = $dataHomImcMayD; 
		$datapage['dataHomImcMayN'] = $dataHomImcMayN; 
		$datapage['dataHomImcMayS'] = $dataHomImcMayS; 

		$datapage['dataHomTPMenD'] = $dataHomTPMenD; 
		$datapage['dataHomTPMenN'] = $dataHomTPMenN;
		$datapage['dataHomTPMenS'] = $dataHomTPMenS; 

		$datapage['dataHomTPMayD'] = $dataHomTPMayD; 
		$datapage['dataHomTPMayN'] = $dataHomTPMayN;
		$datapage['dataHomTPMayS'] = $dataHomTPMayS; 

		$datapage['dataMujImcMenD'] = $dataMujImcMenD; 
		$datapage['dataMujImcMenN'] = $dataMujImcMenN;
		$datapage['dataMujImcMenS'] = $dataMujImcMenS; 

		$datapage['dataMujImcMayD'] = $dataMujImcMayD; 
		$datapage['dataMujImcMayN'] = $dataMujImcMayN; 
		$datapage['dataMujImcMayS'] = $dataMujImcMayS; 

		$datapage['dataMujTPMenD'] = $dataMujTPMenD; 
		$datapage['dataMujTPMenN'] = $dataMujTPMenN; 
		$datapage['dataMujTPMenS'] = $dataMujTPMenS; 

		$datapage['dataMujTPMayD'] = $dataMujTPMayD; 
		$datapage['dataMujTPMayN'] = $dataMujTPMayN; 
		$datapage['dataMujTPMayS'] = $dataMujTPMayS; 

		// Asistencia por grupos
		$datapage['dataAsistenciaArray'] =  array($asistenciaMenHom, $asistenciaMayHom, $asistenciaMenMuj, $asistenciaMayMuj);

		$datapage['dataAsistencia'] =  $asistenciaMenHom.','.$asistenciaMayHom.','.$asistenciaMenMuj.','.$asistenciaMayMuj ;
		$datapage['dataInastsitencia'] =  ($ninosMenCount - $asistenciaMenHom).','.($ninosMayCount - $asistenciaMayHom).','.($ninasMenCount - $asistenciaMenMuj).','.($ninasMayCount - $asistenciaMayMuj);


		$this->load->view('head',$datapage);
		$this->load->view('menu');
		$this->load->view('index');
		$this->load->view('scripts');
	}

	public function AddMedicion()
	{
		$año = intval(date('Y'));
		$Mes = intval(date('m'));

		if(@$_POST['AgregarMedicion'])
        {
            $postdata = $_POST['post'];
            $nino = $this->ModelNiños->getById($postdata->idNiño);
            $postdata['edad'] = $año - intval($nino->fechaNacimiento);
            $postdata['imc'] = $postdata->peso /(($postdata->talla/100)*($postdata->talla/100));
            $postdata['sexo'] = $nino->sexo;
            switch ($Mes) {
				    case 1:
				    	$postdata['periodo'] = 'Bimestre1--'.$año;
				    break;
				    case 2:
				    	$postdata['periodo'] = 'Bimestre1--'.$año;
				    break;
				    case 3:
				    	$postdata['periodo'] = 'Bimestre2--'.$año;
				    break;
				    case 4:
				    	$postdata['periodo'] = 'Bimestre2--'.$año;
				    break;
				    case 5:
				    	$postdata['periodo'] = 'Bimestre3--'.$año;
				    break;
				    case 6:
				    	$postdata['periodo'] = 'Bimestre3--'.$año;
				    break;
				    case 7:
				    	$postdata['periodo'] = 'Bimestre4--'.$año;
				    break;
				    case 8:
				    	$postdata['periodo'] = 'Bimestre4--'.$año;
				    break;
				    case 9:
				    	$postdata['periodo'] = 'Bimestre5--'.$año;
				    break;
				    case 10:
				    	$postdata['periodo'] = 'Bimestre5--'.$año;
				    break;
				    case 11:
				    	$postdata['periodo'] = 'Bimestre6--'.$año;
				    break;
				    case 12:
				    	$postdata['periodo'] = 'Bimestre6--'.$año;
				    break;
			}
            if($nino->sexo == "HOMBRE"){
	            switch ($postdata->edad) {
				    case 1:
				    	$postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 2:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 3:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 4:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 5:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 6:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 7:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 8:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 9:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 10:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 11:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 12:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 13:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 14:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 15:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				}
            }
            else{
            	switch ($postdata->edad) {
				    case 1:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 2:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 3:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 4:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 5:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 6:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 7:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 8:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 9:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 10:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 11:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 12:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 13:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 14:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				    case 15:
				        $postdata['edoIMC'] = 'sobrepeso';
				    	$postdata['edoTallaPeso'] = 'sobrepeso';
				        break;
				}

            }

            $this->ModelMedicion->add($postdata);

        }
        if(@$_POST['AgregarVacios'])
        {
            $postdata = $_POST['post'];
            $productsArray = explode("--",$postdata);
            foreach ($productsArray as $id) 
        	{	
        		$nino = $this->ModelNiños->getById(intval($id));
            	$data['edad'] = $año - intval($nino->fechaNacimiento);
            	$data['idNiño'] = $id;
            	$data['imc'] = 0;
            	$data['talla'] = 0;
            	$data['peso'] = 0;
            	$data['circuBrazo'] = 0;
            	$data['edoIMC'] = 'ND';
            	$data['edoTallaPeso'] = 'ND';
            	$data['sexo'] = $nino->sexo;
            	switch ($Mes) {
				    case 1:
				    	$data['periodo'] = 'Bimestre1--'.$año;
				    break;
				    case 2:
				    	$data['periodo'] = 'Bimestre1--'.$año;
				    break;
				    case 3:
				    	$data['periodo'] = 'Bimestre2--'.$año;
				    break;
				    case 4:
				    	$data['periodo'] = 'Bimestre2--'.$año;
				    break;
				    case 5:
				    	$data['periodo'] = 'Bimestre3--'.$año;
				    break;
				    case 6:
				    	$data['periodo'] = 'Bimestre3--'.$año;
				    break;
				    case 7:
				    	$data['periodo'] = 'Bimestre4--'.$año;
				    break;
				    case 8:
				    	$data['periodo'] = 'Bimestre4--'.$año;
				    break;
				    case 9:
				    	$data['periodo'] = 'Bimestre5--'.$año;
				    break;
				    case 10:
				    	$data['periodo'] = 'Bimestre5--'.$año;
				    break;
				    case 11:
				    	$data['periodo'] = 'Bimestre6--'.$año;
				    break;
				    case 12:
				    	$data['periodo'] = 'Bimestre6--'.$año;
				    break;
				}
				if($this->ModelMedicion->add($data) > 0 ){
					echo $data;
				}
				else{
					echo 'hubo un error';
				}
           	}
        }
		$this->load->view('head');
		$this->load->view('menu');
		$this->load->view('medicion');
		$this->load->view('scripts');
	}
	public function Bimestre($bimestre)
	{
		$ninosList = $this->ModelMedicion->getByBimestre($bimestre);

		$nombre = $bimestre;
		$hombresImcD = 0;
		$hombresImcN= 0;
		$hombresImcS= 0;
		$hombresTallaPesoD= 0;
		$hombresTallaPesoN= 0;
		$hombresTallaPesoS= 0;
		$hombresMenorImcD= 0;
		$hombresMenorImcN= 0;
		$hombresMenorImcS= 0;
		$hombresMenorTallaPesoD= 0;
		$hombresMenorTallaPesoN= 0;
		$hombresMenorTallaPesoS= 0;
		$mujeresImcD= 0;
		$mujeresImcN= 0;
		$mujeresImcS= 0;
		$mujeresTallaPesoD= 0;
		$mujeresTallaPesoN= 0;
		$mujeresTallaPesoS= 0;
		$mujeresMenorImcD= 0;
		$mujeresMenorImcN= 0;
		$mujeresMenorImcS= 0;
		$mujeresMenorTallaPesoD= 0;
		$mujeresMenorTallaPesoN= 0;
		$mujeresMenorTallaPesoS= 0;
		$hombresCirBrazoD= 0;
		$hombresCirBrazoN= 0;
		$hombresCirBrazoS= 0;
		$mujeresCirBrazoD= 0;
		$mujeresCirBrazoN= 0;
		$mujeresCirBrazoS= 0;
		$hombresMenorCirBrazoD= 0;
		$hombresMenorCirBrazoN= 0;
		$hombresMenorCirBrazoS= 0;
		$mujeresMenorCirBrazoD= 0;
		$mujeresMenorCirBrazoN= 0;
		$mujeresMenorCirBrazoS= 0;
		$siAsistenron= 0;
		$noAsistieron= 0;

		foreach ($ninosList as $valor) {
			if($valor->peso != 0){
				$siAsistenron++;
				if( $valor->edad < 5 ){
					if($valor->sexo =="HOMBRE"){

						if($valor->edoIMC == "bajopeso"){
							$hombresMenorImcD++;
						}
						else if($valor->edoIMC == "normal"){
							$hombresMenorImcN++;
						}
						else{
							$hombresMenorImcS++;
						}

						if($valor->edoTallaPeso == "bajopeso"){
							$hombresMenorTallaPesoD++;
						}
						else if($valor->edoTallaPeso == "normal"){
							$hombresMenorTallaPesoN++;
						}
						else{	
							$hombresMenorTallaPesoS++;
						}
					}
					else{
						if($valor->edoIMC == "bajopeso"){
							$mujeresMenorImcD++;
						}
						else if($valor->edoIMC == "normal"){
							$mujeresMenorImcN++;
						}
						else{
							$mujeresMenorImcS++;
						}

						if($valor->edoTallaPeso == "bajopeso"){
							$mujeresMenorTallaPesoD++;
						}
						else if($valor->edoTallaPeso == "normal"){
							$mujeresMenorTallaPesoN++;
						}
						else{	
							$mujeresMenorTallaPesoS++;
						}
					}
				}
				else{
					if($valor->sexo =="HOMBRE"){

						if($valor->edoIMC == "bajopeso"){
							$hombresImcD++;
						}
						else if($valor->edoIMC == "normal"){
							$hombresImcN++;
						}
						else{
							$hombresImcS++;
						}

						if($valor->edoTallaPeso == "bajopeso"){
							$hombresTallaPesoD++;
						}
						else if($valor->edoTallaPeso == "normal"){
							$hombresTallaPesoN++;
						}
						else{	
							$hombresTallaPesoS++;
						}
					}
					else{
						if($valor->edoIMC == "bajopeso"){
							$mujeresImcD++;
						}
						else if($valor->edoIMC == "normal"){
							$mujeresImcN++;
						}
						else{
							$mujeresImcS++;
						}

						if($valor->edoTallaPeso == "bajopeso"){
							$mujeresTallaPesoD++;
						}
						else if($valor->edoTallaPeso == "normal"){
							$mujeresTallaPesoN++;
						}
						else{	
							$mujeresTallaPesoS++;
						}
					}
				}
			}
			else{
				$noAsistieron++;
			}
		}

		$data['nombre'] = $nombre; 
		$data['hombresImcD'] = $hombresImcD;
		$data['hombresImcN'] = $hombresImcN;
		$data['hombresImcS'] = $hombresImcS;
		$data['hombresTallaPesoD'] = $hombresTallaPesoD;
		$data['hombresTallaPesoN'] = $hombresTallaPesoN;
		$data['hombresTallaPesoS'] = $hombresTallaPesoS;
		$data['hombresMenorImcD'] = $hombresMenorImcD;
		$data['hombresMenorImcN'] = $hombresMenorImcN;
		$data['hombresMenorImcS'] = $hombresMenorImcS;
		$data['hombresMenorTallaPesoD'] = $hombresMenorTallaPesoD;
		$data['hombresMenorTallaPesoN'] = $hombresMenorTallaPesoN;
		$data['hombresMenorTallaPesoS'] = $hombresMenorTallaPesoS;
		$data['mujeresImcD'] = $mujeresImcD;
		$data['mujeresImcN'] = $mujeresImcN;
		$data['mujeresImcS'] = $mujeresImcS;
		$data['mujeresTallaPesoD'] = $mujeresTallaPesoD;
		$data['mujeresTallaPesoN'] = $mujeresTallaPesoN;
		$data['mujeresTallaPesoS'] = $mujeresTallaPesoS;
		$data['mujeresMenorImcD'] = $mujeresMenorImcD;
		$data['mujeresMenorImcN'] = $mujeresMenorImcN;
		$data['mujeresMenorImcS'] = $mujeresMenorImcS;
		$data['mujeresMenorTallaPesoD'] = $mujeresMenorTallaPesoD;
		$data['mujeresMenorTallaPesoN'] = $mujeresMenorTallaPesoN;
		$data['mujeresMenorTallaPesoS'] = $mujeresMenorTallaPesoS;
		$data['hombresCirBrazoD'] = $hombresCirBrazoD;
		$data['hombresCirBrazoN'] = $hombresCirBrazoN;
		$data['hombresCirBrazoS'] = $hombresCirBrazoS;
		$data['mujeresCirBrazoD'] = $mujeresCirBrazoD;
		$data['mujeresCirBrazoN'] = $mujeresCirBrazoN;
		$data['mujeresCirBrazoS'] = $mujeresCirBrazoS;
		$data['hombresMenorCirBrazoD'] = $hombresMenorCirBrazoD;
		$data['hombresMenorCirBrazoN'] = $hombresMenorCirBrazoN;
		$data['hombresMenorCirBrazoS'] = $hombresMenorCirBrazoS;
		$data['mujeresMenorCirBrazoD'] = $mujeresMenorCirBrazoD;
		$data['mujeresMenorCirBrazoN'] = $mujeresMenorCirBrazoN;
		$data['mujeresMenorCirBrazoS'] = $mujeresMenorCirBrazoS;
		$data['siAsistenron'] = $siAsistenron;
		$data['noAsistieron'] = $noAsistieron;

		if($this->ModelBimestre->add($data) > 0){
			redirect("Report",'refresh');
		}
		else{
			echo 'hubo un error';
		}
	}
}