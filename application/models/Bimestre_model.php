<?php 
class Bimestre_model extends CI_Model {
 		var $table = "Bimestres";
		public function __construct()	{
		  $this->load->database(); 
		}
		public function getAll(){
	        $q = $this->db->get($this->table);
	        if($q->num_rows() > 0)
	        {
	            return $q->result();
	        }
	        return array();
	    }
	    public function getByUltimos(){
	        $this->db->order_by("id", "desc"); 
	        $q = $this->db->get($this->table,1);
	        if($q->num_rows() > 0)
	        {
	            return $q->result();
	        }
	        return array();
	    }
	    function getById($id)
		{
		    $this->db->where("id",$id);
		    $q = $this->db->get($this->table);
		    if($q->num_rows() > 0)
		    {
		        return $q->row();
		    }
		    return false;
		}

		public function add($data)
	    {
	        $this->db->insert($this->table,$data);
	       	return $this->db->affected_rows();
	    }
		public function update($data,$id)
	    {
	        $this->db->where("ID",$id);
	        $this->db->update($this->table,$data);
	    }

}