<?php 
class Niños_model extends CI_Model {
 		var $table = "Niños";
		public function __construct()	{
		  $this->load->database(); 
		}
		public function getAll(){
	        $q = $this->db->get($this->table);
	        if($q->num_rows() > 0)
	        {
	            return $q->result();
	        }
	        return array();
	    }
	    function getById($id)
		{
		    $this->db->where("id",$id);
		    $q = $this->db->get($this->table);
		    if($q->num_rows() > 0)
		    {
		        return $q->row();
		    }
		    return false;
		}
		function getBySexo($sexo)
		{
		    $this->db->where("sexo",$sexo);
		    $q = $this->db->get($this->table);
		    if($q->num_rows() > 0)
		    {
		        return $q->result();
		    }
		    return false;
		}
		public function add($data)
	    {
	        $this->db->insert($this->table,$data);
	    }
		public function update($data,$id)
	    {
	        $this->db->where("ID",$id);
	        $this->db->update($this->table,$data);
	    }

}
?>
