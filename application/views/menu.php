<body class="sticky-header">

    <section>
        <!-- sidebar left start-->
        <div class="sidebar-left">
            <!--responsive view logo start-->
            <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
                <a href="index.html">
                    <img src="<? echo base_url('Assets/img/logo-icon.png')?>" alt="">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">Report Hope</span>
                </a>
            </div>
            <!--responsive view logo end-->

            <div class="sidebar-left-info">
                <!-- visible small devices start-->
                <div class=" search-field">  </div>
                <!-- visible small devices end-->

                <!--sidebar nav start-->
                <ul class="nav nav-pills nav-stacked side-navigation">
                    <li>
                        <h3 class="navigation-title">Asistencia</h3>
                    </li>
                    <li ><a class="showSection" href="#" data-show="Asistencia"><i class="fa fa-file"></i> <span>Ver Reporte</span></a></li>
                    <li>
                        <h3 class="navigation-title">Indice de Masa Corporal</h3>
                    </li>
                    <li ><a class="showSection" href="#" data-show="IMC"><i class="fa fa-file"></i> <span>Ver Reporte</span></a></li>
                    <li>
                        <h3 class="navigation-title">Tablas Talla-Peso</h3>
                    </li>
                    <li ><a class="showSection" href="#" data-show="TallaPeso"><i class="fa fa-file"></i> <span>Ver Reporte</span></a></li>
                </ul>
                <!--sidebar nav end-->

            </div>
        </div>
        <!-- sidebar left end-->