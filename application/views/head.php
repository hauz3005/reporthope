<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Report Hope</title>
    <link href="<?echo base_url('Assets/js/jquery-easy-pie-chart/jquery.easy-pie-chart.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?echo base_url('Assets/js/vector-map/jquery-jvectormap-1.1.1.css')?>">
    <link href="<?echo base_url('Assets/css/slidebars.css')?>" rel="stylesheet">
    <link href="<?echo base_url('Assets/js/switchery/switchery.min.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?echo base_url('Assets/js/jquery-ui/jquery-ui-1.10.1.custom.min.css')?>" rel="stylesheet" />
    <link href="<?echo base_url('Assets/js/icheck/skins/all.css')?>" rel="stylesheet">
    <link href="<?echo base_url('Assets/css/owl.carousel.css')?>" rel="stylesheet">
    <link href="<?echo base_url('Assets/css/style.css')?>" rel="stylesheet">
    <link href="<?echo base_url('Assets/css/style-responsive.css')?>" rel="stylesheet">
</head>