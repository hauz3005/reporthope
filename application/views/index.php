<div class="body-content" >

            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Reporte De Estado Nutrimental
                </h3>
                <span class="sub-title">Estadisticas Generales Del Periodo:  <?echo $bimestres?></span>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper Asistencia showWrapper">

                <div class="row" style="background-color:white">
                    <h1 class="TituloDivision">Reporte de Asistencia</h1>
                    <hr class="hrTitulo">
                    
                </div>

                <!--state overview start-->
                <div class="container state-overview ParrafoDivision" style="background-color:white;margin-bottom:70px;">
                    <?echo $bimestresList[0]->textoAsistencia;?>
                </div> 
                 <div class="row state-overview" style="background-color:white"> 
                    <div class="col-sm-6 col-lg-3">
                        <section class="panel blue">
                            <div class="symbol">
                                <i class="fa fa-male"></i>
                            </div>
                            <div class="value white">
                                <h1 class="timer" data-from="0" data-to="<?echo $ninosMenCount;?>"
                                    data-speed="1000">
                                </h1>
                                <p>Niños Menores De 5 Años</p>
                            </div>
                        </section>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <section class="panel blue">
                            <div class="symbol ">
                                <i class="fa fa-male"></i>
                            </div>
                            <div class="value white">
                                <h1 class="timer" data-from="0" data-to="<?echo $ninosMayCount;?>"
                                    data-speed="1000">
                                </h1>
                                <p>Niños Mayores De 5 Años</p>
                            </div>
                        </section>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <section class="panel purple">
                            <div class="symbol ">
                                <i class="fa fa-female"></i>
                            </div>
                            <div class="value white">
                                <h1 class="timer" data-from="0" data-to="<?echo $ninasMenCount;?>"
                                    data-speed="1000">
                                    <!--432-->
                                </h1>
                                <p>Niñas Menores De 5 Años</p>
                            </div>
                        </section>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <section class="panel purple">
                            <div class="symbol ">
                                <i class="fa fa-female"></i>
                            </div>
                            <div class="value white">
                                <h1 class="timer" data-from="0" data-to="<?echo $ninasMayCount;?>"
                                    data-speed="3000">
                                    <!--2345-->
                                </h1>
                                <p>Niñas Mayores De 5 Años</p>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="row state-overview" style="background-color:white"> 
                    <div class="col-sm-10 col-sm-offset-1 col-lg-6 col-lg-offset-0">
                        <section class="panel" style="padding:50px;">
                            <header class="panel-heading head-border">
                                Asistencia
                            </header>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Asistencia</th>
                                    <th>Inasistencia</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Niños < 5</td>
                                    <td><?echo $dataAsistenciaArray[0]?></td>
                                    <td><?echo $ninosMenCount-$dataAsistenciaArray[0]?></td>
                                    <td><?echo $ninosMenCount?></td>
                                </tr>
                                <tr>
                                    <td>Niños > 5</td>
                                    <td><?echo $dataAsistenciaArray[1]?></td>
                                    <td><?echo $ninosMayCount-$dataAsistenciaArray[1]?></td>
                                    <td><?echo $ninosMayCount?></td>
                                </tr>
                                <tr>
                                    <td>Niñas < 5</td>
                                    <td><?echo $dataAsistenciaArray[2]?></td>
                                    <td><?echo $ninasMenCount-$dataAsistenciaArray[2]?></td>
                                    <td><?echo $ninasMenCount?></td>
                                </tr>
                                <tr>
                                    <td>Niñas > 5</td>
                                    <td><?echo $dataAsistenciaArray[3]?></td>
                                    <td><?echo $ninasMayCount-$dataAsistenciaArray[3]?></td>
                                    <td><?echo $ninasMayCount?></td>
                                </tr>
                                </tbody>
                            </table>
                        </section>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1 col-lg-6 col-lg-offset-0">
                        <section class="panel">
                            <header class="panel-heading">
                                Asistencia de niños a la medicion
                            </header>
                            <div class="panel-body">
                                <div class="chartJS">
                                    <canvas id="AsistenciaData" height="240" width="100%" ></canvas>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
               
                <!--state overview end-->

            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            

<!--             <div class="page-head">
                <h3>
                    Reporte De Estado Nutrimental
                </h3>
                <span class="sub-title">Estadisticas Generales Del Periodo:  <?echo $bimestres?></span>
            </div> -->
            
            <div class="wrapper IMC">
                <div class="row" style="background-color:white">
                    <h1 class="TituloDivision">Evaluacion Por Medio Indice de Masa Corporal (IMC)</h1>
                    <hr class="hrTitulo">
                </div>
                <div class="container state-overview ParrafoDivision" style="background-color:white;margin-bottom:40px;">
                    <?echo $bimestresList[0]->textoIMC?>
                </div>
                <div class="row" style="background-color:white">
                    <h3 class="TituloDivision">Estado Nutrimental de Niños</h3>                    
                </div> 
                <div class="row" >
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Estado Nutrimental De Niños Menores de 5 Años
                            </header>
                            <div class="panel-body">
                                <div class="chartJS">
                                    <canvas id="IMC-hombres-menores" height="240" width="100%" ></canvas>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Estado Nutrimental De Niños Mayores de 5 Años
                            </header>
                            <div class="panel-body">
                                <div class="chartJS">
                                    <canvas id="IMC-hombres" height="240" width="100%" ></canvas>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="row" style="background-color:white">
                    <h3 class="TituloDivision">Estado Nutrimental de Niñas</h3>                    
                </div>
                <div class="row">
                    
                    
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                 Estado Nutrimental De Niñas Menores de 5 Años
                            </header>
                            <div class="panel-body">
                                <div class="chartJS">
                                    <canvas id="IMC-mujeres-menores" height="240" width="100%" ></canvas>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                 Estado Nutrimental De Niñas Mayores de 5 Años
                            </header>
                            <div class="panel-body">
                                <div class="chartJS">
                                    <canvas id="IMC-mujeres" height="240" width="100%" ></canvas>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>


                <div class="container ">
                    <div class="centerdiv">
                        <div class="color">
                            <div class="amarillo"></div>
                            Niños con bajo peso.
                        </div>
                        <div class="color">
                            <div class="verde"></div>
                            Niños con peso normal.
                        </div>
                        <div class="color">
                            <div class="naranja"></div>
                           Niños con sobre peso.
                        </div>
                    </div>
                    
                </div>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            

<!--             <div class="page-head">
                <h3>
                    Reporte De Estado Nutrimental
                </h3>
                <span class="sub-title">Estadisticas Generales Del Periodo:  <?echo $bimestres?></span>
            </div> -->
            
            <div class="wrapper TallaPeso">

                <div class="row" style="background-color:white">
                    <h1 class="TituloDivision">Evaluacion Por Medio De Tablas  Talla-Peso Para La Edad</h1>
                    <hr class="hrTitulo">
                    
                </div>

            <div class="container state-overview ParrafoDivision" style="background-color:white;margin-bottom:70px;">
                    <?echo $bimestresList[0]->textoTP?>

                </div> 



                <div class="row">
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Estado Nutrimental De Niños Menores de 5 Años
                            </header>
                            <div class="panel-body">
                                <div class="chartJS">
                                    <canvas id="TP-hombres-menores" height="240" width="100%" ></canvas>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                 Estado Nutrimental De Niñas Menores de 5 Años
                            </header>
                            <div class="panel-body">
                                <div class="chartJS">
                                    <canvas id="TP-mujeres-menores" height="240" width="100%" ></canvas>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                Estado Nutrimental De Niños Mayores de 5 Años
                            </header>
                            <div class="panel-body">
                                <div class="chartJS">
                                    <canvas id="TP-hombres" height="240" width="100%" ></canvas>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-md-6">
                        <section class="panel">
                            <header class="panel-heading">
                                 Estado Nutrimental De Niñas Mayores de 5 Años
                            </header>
                            <div class="panel-body">
                                <div class="chartJS">
                                    <canvas id="TP-mujeres" height="240" width="100%" ></canvas>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>


                <div class="container ">
                    <div class="centerdiv">
                        <div class="color">
                            <div class="amarillo"></div>
                            Niños con bajo peso.
                        </div>
                        <div class="color">
                            <div class="verde"></div>
                            Niños con peso normal.
                        </div>
                        <div class="color">
                            <div class="naranja"></div>
                           Niños con sobre peso.
                        </div>
                    </div>
                    
                </div>
   




            </div>
            <!--body wrapper end-->


            <!--footer section start-->
            <footer>
                2016 &copy; Lincenciada en Nutricion Georgina Hurtado Mendoza.
            </footer>
            <!--footer section end-->



        </div>

        <script type="text/javascript">
              var AsistenciaData = {
                labels : ["Niños < 5","Niños > 5","Niñas < 5","Niñas > 5"],
                datasets : [
                    {
                        fillColor : "#4EC9B4",
                        strokeColor : "#4EC9B4",
                        data : [<?echo $dataAsistencia ?>]
                    },
                    {
                        fillColor : "#e55957",
                        strokeColor : "#e55957",
                        data : [<?echo $dataInastsitencia ?>]
                    },
                ]
            };


            var IMChombresmenores = {
                labels : [<?echo $bimestres?>],
                datasets : [
                    {
                        fillColor : "#ffea80",
                        strokeColor : "#ffea80",
                        data : [<?echo $dataHomImcMenD ?>]
                    },
                    {
                        fillColor : "#4EC9B4",
                        strokeColor : "#4EC9B4",
                        data : [<?echo $dataHomImcMenN ?>]
                    },
                    {
                        fillColor : "#FF834D",
                        strokeColor : "#FF834D",
                        data : [<?echo $dataHomImcMenS ?>]
                    }
                ]
            };
            var IMCmujeresmenores = {
                labels : [<?echo $bimestres?>],
                datasets : [
                    {
                        fillColor : "#ffea80",
                        strokeColor : "#ffea80",
                        data : [<?echo $dataMujImcMenD ?>]
                    },
                    {
                        fillColor : "#4EC9B4",
                        strokeColor : "#4EC9B4",
                        data : [<?echo $dataMujImcMenN ?>]
                    },
                    {
                        fillColor : "#FF834D",
                        strokeColor : "#FF834D",
                        data : [<?echo $dataMujImcMenS ?>]
                    }
                ]
            };
            var IMChombres = {
                labels : [<?echo $bimestres?>],
                datasets : [
                    {
                        fillColor : "#ffea80",
                        strokeColor : "#ffea80",
                        data : [<?echo $dataHomImcMayD ?>]
                    },
                    {
                        fillColor : "#4EC9B4",
                        strokeColor : "#4EC9B4",
                        data : [<?echo $dataHomImcMayN ?>]
                    },
                    {
                        fillColor : "#FF834D",
                        strokeColor : "#FF834D",
                        data : [<?echo $dataHomImcMayS ?>]
                    }
                ]
            };
            var IMCmujeres = {
                labels : [<?echo $bimestres?>],
                datasets : [
                    {
                        fillColor : "#ffea80",
                        strokeColor : "#ffea80",
                        data : [<?echo $dataMujImcMayD ?>]
                    },
                    {
                        fillColor : "#4EC9B4",
                        strokeColor : "#4EC9B4",
                        data : [<?echo $dataMujImcMayN ?>]
                    },
                    {
                        fillColor : "#FF834D",
                        strokeColor : "#FF834D",
                        data : [<?echo $dataMujImcMayS ?>]
                    }
                ]
            };
            var TPhombresmenores = {
                labels : [<?echo $bimestres?>],
                datasets : [
                    {
                        fillColor : "#ffea80",
                        strokeColor : "#ffea80",
                        data : [<?echo $dataHomTPMenD ?>]
                    },
                    {
                        fillColor : "#4EC9B4",
                        strokeColor : "#4EC9B4",
                        data : [<?echo $dataHomTPMenN ?>]
                    },
                    {
                        fillColor : "#FF834D",
                        strokeColor : "#FF834D",
                        data : [<?echo $dataHomTPMenS ?>]
                    }
                ]
            };
            var TPmujeresmenores = {
                labels : [<?echo $bimestres?>],
                datasets : [
                    {
                        fillColor : "#ffea80",
                        strokeColor : "#ffea80",
                        data : [<?echo $dataMujTPMenD ?>]
                    },
                    {
                        fillColor : "#4EC9B4",
                        strokeColor : "#4EC9B4",
                        data : [<?echo $dataMujTPMenN ?>]
                    },
                    {
                        fillColor : "#FF834D",
                        strokeColor : "#FF834D",
                        data : [<?echo $dataMujTPMenS ?>]
                    }
                ]
            };
            var TPhombres = {
                labels : [<?echo $bimestres?>],
                datasets : [
                    {
                        fillColor : "#ffea80",
                        strokeColor : "#ffea80",
                        data : [<?echo $dataHomTPMayD ?>]
                    },
                    {
                        fillColor : "#4EC9B4",
                        strokeColor : "#4EC9B4",
                        data : [<?echo $dataHomTPMayN ?>]
                    },
                    {
                        fillColor : "#FF834D",
                        strokeColor : "#FF834D",
                        data : [<?echo $dataHomTPMayS ?>]
                    }
                ]
            };
            var TPmujeres = {
                labels : [<?echo $bimestres?>],
                datasets : [
                    {
                        fillColor : "#ffea80",
                        strokeColor : "#ffea80",
                        data : [<?echo $dataMujTPMayD ?>]
                    },
                    {
                        fillColor : "#4EC9B4",
                        strokeColor : "#4EC9B4",
                        data : [<?echo $dataMujTPMayN ?>]
                    },
                    {
                        fillColor : "#FF834D",
                        strokeColor : "#FF834D",
                        data : [<?echo $dataMujTPMayS ?>]
                    }
                ]
            };
        </script>