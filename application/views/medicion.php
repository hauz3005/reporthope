<div class="body-content" >

            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Captura de Mediciones
                </h3>
                <span class="sub-title">Ingresar Nuevas captura de mediciones</span>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper ">

                <div class="row" style="background-color:white">
                    <h1 class="TituloDivision">Captura de Mediciones</h1>
                    <hr class="hrTitulo">
                </div>

                <!--state overview start-->
                <div class="container state-overview ParrafoDivision" style="background-color:white;margin-bottom:70px;">
                    <form id="contact-form" method="post" >                    
                
                        <div class="form-group col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12"> 
                            <input type="text" class="form-control" placeholder="Id de Niño" name="post[idNiño]" required="">
                        </div>
                        <div class="form-group col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                            <input type="email" class="form-control" placeholder="Talla" name="post[talla]" required="">
                        </div>
                        <div class="form-group col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12"> 
                            <input type="text" class="form-control" placeholder="Peso" name="post[peso]" required="">
                        </div>
                        <div class="form-group col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                            <input type="text" class="form-control" placeholder="Circuferencia del Brazo" name="post[circuBrazo]" required="">
                        </div>

                        <div id="btn-overlay" class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                            <input type="submit" class="btn btn-default col-xs-12" value="Agregar" name="AgregarMedicion">
                        </div>

                                     
                    </form>
                </div> 

                <div class="row" style="background-color:white">
                    <h1 class="TituloDivision">Captura de Mediciones De ID Vacios</h1>
                    <hr class="hrTitulo">
                </div>

                <!--state overview start-->
                <div class="container state-overview ParrafoDivision" style="background-color:white;margin-bottom:70px;">
                    <form id="contact-form" method="post" >                    
                    
                            <div class="form-group col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                                <textarea class="form-control" rows="3" placeholder="Id de Niños que no Asistieron Escribelos asi Ejemplo: 3--24--35--44--...--201" name="post" required=""></textarea>
                            </div>
                            <div id="btn-overlay" class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                                <!-- <a href="#">Send</a>  -->
                                <input type="submit" class="btn btn-default col-xs-12" value="No Asistieron" name="AgregarVacios">
                            </div>

                                         
                        </form>
                </div> 

                <!--state overview end-->

            </div>
            <!-- page head end-->

   


            <!--footer section start-->
            <footer>
                2016 &copy; Lincenciada en Nutricion Georgina Hurtado Mendoza.
            </footer>
            <!--footer section end-->



        </div>
