  </section>



<!-- Placed js at the end of the document so the pages load faster -->
<script src="<? echo base_url('Assets/js/jquery-1.10.2.min.js')?>"></script>

<!--jquery-ui-->
<script src="<? echo base_url('Assets/js/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript')?>"></script>

<script src="<? echo base_url('Assets/js/jquery-migrate.js')?>"></script>
<script src="<? echo base_url('Assets/js/bootstrap.min.js')?>"></script>
<script src="<? echo base_url('Assets/js/modernizr.min.js')?>"></script>

<!--Nice Scroll-->
<script src="<? echo base_url('Assets/js/jquery.nicescroll.js" type="text/javascript')?>"></script>

<!--right slidebar-->
<script src="<? echo base_url('Assets/js/slidebars.min.js')?>"></script>

<!--switchery-->
<script src="<? echo base_url('Assets/js/switchery/switchery.min.js')?>"></script>
<script src="<? echo base_url('Assets/js/switchery/switchery-init.js')?>"></script>

<!--flot chart -->
<script src="<? echo base_url('Assets/js/flot-chart/jquery.flot.js')?>"></script>
<script src="<? echo base_url('Assets/js/flot-chart/flot-spline.js')?>"></script>
<script src="<? echo base_url('Assets/js/flot-chart/jquery.flot.resize.js')?>"></script>
<script src="<? echo base_url('Assets/js/flot-chart/jquery.flot.tooltip.min.js')?>"></script>
<script src="<? echo base_url('Assets/js/flot-chart/jquery.flot.pie.js')?>"></script>
<script src="<? echo base_url('Assets/js/flot-chart/jquery.flot.selection.js')?>"></script>
<script src="<? echo base_url('Assets/js/flot-chart/jquery.flot.stack.js')?>"></script>
<script src="<? echo base_url('Assets/js/flot-chart/jquery.flot.crosshair.js')?>"></script>

<!--Chart JS-->
<script src="<? echo base_url('Assets/js/chart-js/chart.js')?>"></script>
<script src="<? echo base_url('Assets/js/chartJs-init.js')?>"></script>

<!--earning chart init-->
<script src="<? echo base_url('Assets/js/earning-chart-init.js')?>"></script>


<!--Sparkline Chart-->
<script src="<? echo base_url('Assets/js/sparkline/jquery.sparkline.js')?>"></script>
<script src="<? echo base_url('Assets/js/sparkline/sparkline-init.js')?>"></script>

<!--easy pie chart-->
<script src="<? echo base_url('Assets/js/jquery-easy-pie-chart/jquery.easy-pie-chart.js')?>"></script>
<script src="<? echo base_url('Assets/js/easy-pie-chart.js')?>"></script>


<!--vectormap-->
<script src="<? echo base_url('Assets/js/vector-map/jquery-jvectormap-1.2.2.min.js')?>"></script>
<script src="<? echo base_url('Assets/js/vector-map/jquery-jvectormap-world-mill-en.js')?>"></script>
<script src="<? echo base_url('Assets/js/dashboard-vmap-init.js')?>"></script>

<!--Icheck-->
<script src="<? echo base_url('Assets/js/icheck/skins/icheck.min.js')?>"></script>
<script src="<? echo base_url('Assets/js/todo-init.js')?>"></script>

<!--jquery countTo-->
<script src="<? echo base_url('Assets/js/jquery-countTo/jquery.countTo.js"  type="text/javascript')?>"></script>

<!--owl carousel-->
<script src="<? echo base_url('Assets/js/owl.carousel.js')?>"></script>


<!--common scripts for all pages-->

<script src="<? echo base_url('Assets/js/scripts.js')?>"></script>


<script type="text/javascript">

    $(document).ready(function() {

        //countTo

        $('.timer').countTo();

        //owl carousel

        $("#news-feed").owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
    });

    $(window).on("resize",function(){
        var owl = $("#news-feed").data("owlCarousel");
        owl.reinit();
    });

</script>

</body>
</html>
