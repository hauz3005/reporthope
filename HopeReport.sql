-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:8889
-- Tiempo de generación: 16-03-2016 a las 19:47:36
-- Versión del servidor: 5.5.42
-- Versión de PHP: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `HopeReport`
--
CREATE DATABASE IF NOT EXISTS `HopeReport` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `HopeReport`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Bimestres`
--

DROP TABLE IF EXISTS `Bimestres`;
CREATE TABLE IF NOT EXISTS `Bimestres` (
  `id` int(4) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `hombresImcD` int(4) DEFAULT NULL,
  `hombresImcN` int(4) DEFAULT NULL,
  `hombresImcS` int(4) DEFAULT NULL,
  `hombresTallaPesoD` int(4) DEFAULT NULL,
  `hombresTallaPesoN` int(4) DEFAULT NULL,
  `hombresTallaPesoS` int(4) DEFAULT NULL,
  `hombresMenorImcD` int(4) DEFAULT NULL,
  `hombresMenorImcN` int(4) DEFAULT NULL,
  `hombresMenorImcS` int(4) DEFAULT NULL,
  `hombresMenorTallaPesoD` int(4) DEFAULT NULL,
  `hombresMenorTallaPesoN` int(4) DEFAULT NULL,
  `hombresMenorTallaPesoS` int(4) DEFAULT NULL,
  `mujeresImcD` int(4) DEFAULT NULL,
  `mujeresImcN` int(4) DEFAULT NULL,
  `mujeresImcS` int(4) DEFAULT NULL,
  `mujeresTallaPesoD` int(4) DEFAULT NULL,
  `mujeresTallaPesoN` int(4) DEFAULT NULL,
  `mujeresTallaPesoS` int(4) DEFAULT NULL,
  `mujeresMenorImcD` int(4) DEFAULT NULL,
  `mujeresMenorImcN` int(4) DEFAULT NULL,
  `mujeresMenorImcS` int(4) DEFAULT NULL,
  `mujeresMenorTallaPesoD` int(4) DEFAULT NULL,
  `mujeresMenorTallaPesoN` int(4) DEFAULT NULL,
  `mujeresMenorTallaPesoS` int(4) DEFAULT NULL,
  `hombresCirBrazoD` int(4) NOT NULL DEFAULT '0',
  `hombresCirBrazoN` int(4) NOT NULL DEFAULT '0',
  `hombresCirBrazoS` int(4) NOT NULL DEFAULT '0',
  `mujeresCirBrazoD` int(4) NOT NULL DEFAULT '0',
  `mujeresCirBrazoN` int(4) NOT NULL DEFAULT '0',
  `mujeresCirBrazoS` int(4) NOT NULL DEFAULT '0',
  `hombresMenorCirBrazoD` int(4) NOT NULL,
  `hombresMenorCirBrazoN` int(4) NOT NULL,
  `hombresMenorCirBrazoS` int(4) NOT NULL,
  `mujeresMenorCirBrazoD` int(4) NOT NULL,
  `mujeresMenorCirBrazoN` int(4) NOT NULL,
  `mujeresMenorCirBrazoS` int(4) NOT NULL,
  `siAsistenron` int(4) DEFAULT NULL,
  `noAsistieron` int(4) DEFAULT NULL,
  `textoAsistencia` text COLLATE utf8_bin NOT NULL,
  `textoIMC` text COLLATE utf8_bin NOT NULL,
  `textoTP` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `Bimestres`
--

INSERT INTO `Bimestres` (`id`, `nombre`, `hombresImcD`, `hombresImcN`, `hombresImcS`, `hombresTallaPesoD`, `hombresTallaPesoN`, `hombresTallaPesoS`, `hombresMenorImcD`, `hombresMenorImcN`, `hombresMenorImcS`, `hombresMenorTallaPesoD`, `hombresMenorTallaPesoN`, `hombresMenorTallaPesoS`, `mujeresImcD`, `mujeresImcN`, `mujeresImcS`, `mujeresTallaPesoD`, `mujeresTallaPesoN`, `mujeresTallaPesoS`, `mujeresMenorImcD`, `mujeresMenorImcN`, `mujeresMenorImcS`, `mujeresMenorTallaPesoD`, `mujeresMenorTallaPesoN`, `mujeresMenorTallaPesoS`, `hombresCirBrazoD`, `hombresCirBrazoN`, `hombresCirBrazoS`, `mujeresCirBrazoD`, `mujeresCirBrazoN`, `mujeresCirBrazoS`, `hombresMenorCirBrazoD`, `hombresMenorCirBrazoN`, `hombresMenorCirBrazoS`, `mujeresMenorCirBrazoD`, `mujeresMenorCirBrazoN`, `mujeresMenorCirBrazoS`, `siAsistenron`, `noAsistieron`, `textoAsistencia`, `textoIMC`, `textoTP`) VALUES
(1, 'Bimestre1--2016', 16, 37, 12, 17, 37, 11, 2, 11, 3, 3, 10, 3, 21, 41, 15, 22, 45, 10, 4, 10, 10, 5, 10, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 182, 33, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet nibh ac neque imperdiet ultrices sit amet in lectus. Vestibulum eget gravida justo. Nulla rutrum ipsum ut diam sagittis malesuada. Pellentesque luctus sed lorem id aliquam. Nullam a aliquet velit. Nullam ac leo dapibus nisi eleifend efficitur. Nulla facilisi. Donec cursus purus nec hendrerit convallis. Suspendisse elementum tellus ut mi feugiat, at imperdiet arcu sollicitudin.  Vestibulum arcu odio, sodales vel dui at, consectetur volutpat augue. Aliquam tempus tortor in magna vulputate, scelerisque pulvinar dolor faucibus. Duis suscipit leo sem, a porta massa viverra sit amet. Maecenas neque orci, tempus non vehicula non, porttitor id est. Vestibulum non elementum tellus.<br><br> Cras tincidunt mollis est, a elementum neque sodales in. Donec mattis eleifend varius. Cras mollis est sed metus mollis, vitae gravida leo pretium. Pellentesque sit amet tincidunt mi. Nulla fermentum mollis sodales. Integer non magna vitae eros hendrerit mollis. Suspendisse potenti. Nam vitae sapien vel metus scelerisque pellentesque quis ut dui. Nullam ante lacus, luctus at erat quis, imperdiet interdum dui. Ut luctus accumsan velit, a viverra augue ultrices ut', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet nibh ac neque imperdiet ultrices sit amet in lectus. Vestibulum eget gravida justo. Nulla rutrum ipsum ut diam sagittis malesuada. Pellentesque luctus sed lorem id aliquam. Nullam a aliquet velit. Nullam ac leo dapibus nisi eleifend efficitur. Nulla facilisi. Donec cursus purus nec hendrerit convallis. Suspendisse elementum tellus ut mi feugiat, at imperdiet arcu sollicitudin.  Vestibulum arcu odio, sodales vel dui at, consectetur volutpat augue. Aliquam tempus tortor in magna vulputate, scelerisque pulvinar dolor faucibus.<br><br> Duis suscipit leo sem, a porta massa viverra sit amet. Maecenas neque orci, tempus non vehicula non, porttitor id est. Vestibulum non elementum tellus. Cras tincidunt mollis est, a elementum neque sodales in. Donec mattis eleifend varius. Cras mollis est sed metus mollis, vitae gravida leo pretium. Pellentesque sit amet tincidunt mi. Nulla fermentum mollis sodales. Integer non magna vitae eros hendrerit mollis. Suspendisse potenti. Nam vitae sapien vel metus scelerisque pellentesque quis ut dui. Nullam ante lacus, luctus at erat quis, imperdiet interdum dui. Ut luctus accumsan velit, a viverra augue ultrices ut', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet nibh ac neque imperdiet ultrices sit amet in lectus. Vestibulum eget gravida justo. Nulla rutrum ipsum ut diam sagittis malesuada. Pellentesque luctus sed lorem id aliquam. Nullam a aliquet velit. Nullam ac leo dapibus nisi eleifend efficitur. Nulla facilisi. Donec cursus purus nec hendrerit convallis. Suspendisse elementum tellus ut mi feugiat, at imperdiet arcu sollicitudin.  Vestibulum arcu odio, sodales vel dui at, consectetur volutpat augue. Aliquam tempus tortor in magna vulputate, scelerisque pulvinar dolor faucibus. Duis suscipit leo sem, a porta massa viverra sit amet. <br><br>Maecenas neque orci, tempus non vehicula non, porttitor id est. Vestibulum non elementum tellus. Cras tincidunt mollis est, a elementum neque sodales in. Donec mattis eleifend varius. Cras mollis est sed metus mollis, vitae gravida leo pretium. Pellentesque sit amet tincidunt mi. Nulla fermentum mollis sodales. Integer non magna vitae eros hendrerit mollis. Suspendisse potenti. Nam vitae sapien vel metus scelerisque pellentesque quis ut dui. Nullam ante lacus, luctus at erat quis, imperdiet interdum dui. Ut luctus accumsan velit, a viverra augue ultrices ut');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Medicion`
--

DROP TABLE IF EXISTS `Medicion`;
CREATE TABLE IF NOT EXISTS `Medicion` (
  `id` int(11) NOT NULL,
  `idNiño` int(4) DEFAULT NULL,
  `edad` int(2) DEFAULT NULL,
  `talla` int(3) DEFAULT NULL,
  `peso` int(3) DEFAULT NULL,
  `circuBrazo` int(3) NOT NULL,
  `imc` float DEFAULT NULL,
  `edoIMC` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `edoTallaPeso` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `periodo` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `sexo` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `Medicion`
--

INSERT INTO `Medicion` (`id`, `idNiño`, `edad`, `talla`, `peso`, `circuBrazo`, `imc`, `edoIMC`, `edoTallaPeso`, `periodo`, `sexo`) VALUES
(1, 1, 11, 47, 146, 0, 22.2368, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(2, 2, 4, 17, 108, 0, 14.489, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(3, 3, 3, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(4, 4, 1, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(5, 5, 10, 29, 133, 0, 16.6205, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(6, 6, 2, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(7, 7, 9, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(8, 8, 11, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(9, 9, 0, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(10, 10, 10, 32, 131, 0, 18.3556, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(11, 11, 2, 14, 94, 0, 15.9574, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(12, 12, 8, 27, 127, 0, 16.74, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(13, 13, 5, 17, 110, 0, 14.0496, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(14, 14, 3, 14, 94, 0, 16.297, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(15, 15, 2, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(16, 16, 2, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(17, 17, 8, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(18, 18, 6, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(19, 19, 10, 32, 136, 0, 17.3551, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(20, 20, 3, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(21, 21, 9, 28, 131, 0, 16.0247, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(22, 22, 12, 45, 150, 0, 19.8222, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(23, 23, 9, 32, 131, 0, 18.6469, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(24, 24, 3, 15, 99, 0, 15.6107, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(25, 25, 12, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(26, 26, 11, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(27, 27, 2, 0, 0, 0, 0, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(28, 28, 2, 14, 91, 0, 17.0269, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(29, 29, 5, 17, 112, 0, 13.5523, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(30, 30, 12, 34, 154, 0, 14.4206, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(31, 31, 10, 29, 145, 0, 13.6029, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'MUJER'),
(32, 32, 7, 20, 120, 0, 13.6111, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(33, 33, 5, 27, 110, 0, 22.0661, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(34, 34, 9, 30, 133, 0, 17.0728, 'sobrepeso', 'sobrepeso', 'Bimestre1--2016', 'HOMBRE'),
(35, 35, 12, 31, 135, 0, 16.845, 'sobrepeso', 'normal', 'Bimestre1--2016', 'MUJER'),
(36, 36, 7, 24, 124, 0, 15.8039, 'sobrepeso', 'normal', 'Bimestre1--2016', 'MUJER'),
(37, 37, 9, 30, 132, 0, 17.4472, 'sobrepeso', 'normal', 'Bimestre1--2016', 'MUJER'),
(38, 38, 10, 0, 0, 0, 0, 'sobrepeso', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(39, 39, 5, 17, 106, 0, 15.3079, 'sobrepeso', 'normal', 'Bimestre1--2016', 'MUJER'),
(40, 40, 3, 13, 93, 0, 14.915, 'sobrepeso', 'normal', 'Bimestre1--2016', 'MUJER'),
(41, 41, 0, 0, 0, 0, 0, 'sobrepeso', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(42, 42, 0, 0, 0, 0, 0, 'sobrepeso', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(43, 43, 9, 31, 138, 0, 16.4881, 'sobrepeso', 'normal', 'Bimestre1--2016', 'MUJER'),
(44, 44, 8, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(45, 45, 11, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(46, 46, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(47, 47, 11, 43, 139, 0, 22.1521, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(48, 48, 7, 19, 119, 0, 13.629, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(49, 49, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(50, 50, 11, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(51, 51, 10, 37, 143, 0, 18.2894, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(52, 52, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(53, 53, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(54, 54, 11, 33, 146, 0, 15.4813, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(55, 55, 9, 28, 132, 0, 16.0698, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(56, 56, 3, 12, 90, 0, 14.321, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(57, 57, 11, 31, 143, 0, 15.013, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(58, 58, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(59, 59, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(60, 60, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(61, 61, 10, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(62, 62, 6, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(63, 63, 2, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(64, 64, 9, 28, 134, 0, 15.4823, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(65, 65, 4, 18, 105, 0, 15.873, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(66, 66, 10, 27, 136, 0, 14.4896, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(67, 67, 12, 51, 156, 0, 20.9977, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(68, 68, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(69, 69, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(70, 70, 7, 21, 119, 0, 14.6176, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(71, 71, 9, 31, 123, 0, 20.6226, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(72, 72, 11, 58, 148, 0, 26.4792, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(73, 73, 12, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(74, 74, 1, 10, 81, 0, 15.0892, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(75, 75, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(76, 76, 3, 12, 88, 0, 15.4959, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(77, 77, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(78, 78, 3, 16, 85, 0, 21.5917, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(79, 79, 6, 23, 117, 0, 17.021, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(80, 80, 5, 18, 107, 0, 15.6346, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(81, 81, 7, 23, 122, 0, 15.6544, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(82, 82, 5, 16, 104, 0, 14.3306, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(83, 83, 11, 34, 143, 0, 16.5778, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(84, 84, 5, 21, 119, 0, 14.6882, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(85, 85, 4, 16, 102, 0, 14.9942, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(86, 86, 5, 16, 106, 0, 14.1509, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(87, 87, 4, 20, 113, 0, 15.7412, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(88, 88, 10, 63, 157, 0, 25.4777, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(89, 89, 8, 36, 136, 0, 19.6259, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(90, 90, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(91, 91, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(92, 92, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(93, 93, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(94, 94, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(95, 95, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(96, 96, 13, 44, 159, 0, 17.2066, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(97, 97, 12, 52, 158, 0, 20.7499, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(98, 98, 8, 34, 129, 0, 20.5516, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(99, 99, 8, 26, 128, 0, 15.564, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(100, 100, 10, 38, 142, 0, 19.0438, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(101, 101, 9, 32, 132, 0, 18.4229, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(102, 102, 4, 17, 103, 0, 15.6471, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(103, 103, 11, 45, 151, 0, 19.6921, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(104, 104, 2, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(105, 105, 10, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(106, 106, 8, 31, 129, 0, 18.809, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(107, 107, 10, 38, 137, 0, 20.406, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(108, 108, 8, 25, 130, 0, 14.6154, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(109, 109, 7, 22, 124, 0, 14.4381, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(110, 110, 2, 18, 110, 0, 14.9587, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(111, 111, 4, 15, 101, 0, 14.9005, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(112, 112, 5, 15, 85, 0, 20.4844, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(113, 113, 5, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(114, 114, 3, 13, 94, 0, 14.9389, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(115, 115, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(116, 116, 11, 28, 137, 0, 14.8649, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(117, 117, 13, 42, 142, 0, 20.6308, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(118, 118, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(119, 119, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(120, 120, 9, 29, 130, 0, 16.9822, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(121, 121, 3, 14, 81, 0, 21.7955, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(122, 122, 1, 10, 81, 0, 15.6988, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(123, 123, 2, 12, 92, 0, 14.6503, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(124, 124, 7, 29, 127, 0, 18.104, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(125, 125, 8, 25, 130, 0, 14.5562, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(126, 126, 12, 38, 151, 0, 16.4905, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(127, 127, 4, 14, 103, 0, 12.8193, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(128, 128, 10, 25, 136, 0, 13.6786, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(129, 129, 9, 28, 130, 0, 16.568, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(130, 130, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(131, 131, 9, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(132, 132, 12, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(133, 133, 11, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(134, 134, 7, 22, 125, 0, 14.336, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(135, 135, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(136, 136, 8, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(137, 137, 11, 31, 141, 0, 15.7437, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(138, 138, 6, 20, 120, 0, 13.6806, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(139, 139, 10, 34, 141, 0, 16.9006, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(140, 140, 2, 14, 96, 0, 15.5165, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(141, 141, 8, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(142, 142, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(143, 143, 7, 29, 130, 0, 16.9822, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(144, 144, 6, 24, 119, 0, 17.0892, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(145, 145, 6, 23, 120, 0, 16.1806, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(146, 146, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(147, 147, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(148, 148, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(149, 149, 8, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(150, 150, 11, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(151, 151, 12, 40, 143, 0, 19.4631, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(152, 152, 7, 22, 120, 0, 15.4861, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(153, 153, 10, 28, 130, 0, 16.3905, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(154, 154, 11, 31, 142, 0, 15.3739, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(155, 155, 7, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(156, 156, 11, 28, 133, 0, 15.9421, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(157, 157, 8, 25, 128, 0, 15.4419, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(158, 158, 13, 46, 151, 0, 20.2184, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(159, 159, 4, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(160, 160, 7, 21, 121, 0, 14.4116, 'normal', 'normal', 'Bimestre1--2016', 'HOMBRE'),
(161, 161, 10, 30, 142, 0, 14.9276, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(162, 162, 9, 28, 132, 0, 15.8402, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(163, 163, 8, 29, 129, 0, 17.547, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(164, 164, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(165, 165, 0, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(166, 166, 8, 0, 0, 0, 0, 'normal', 'normal', 'Bimestre1--2016', 'MUJER'),
(167, 167, 8, 22, 123, 0, 14.4094, 'normal', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(168, 168, 4, 17, 105, 0, 15.0567, 'normal', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(169, 169, 8, 24, 134, 0, 13.2546, 'normal', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(170, 170, 3, 14, 95, 0, 15.1801, 'normal', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(171, 171, 7, 26, 133, 0, 14.7549, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(172, 172, 10, 33, 136, 0, 17.8417, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(173, 173, 8, 28, 130, 0, 16.3905, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(174, 174, 12, 34, 148, 0, 15.431, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(175, 175, 9, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(176, 176, 6, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(177, 177, 8, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(178, 178, 5, 19, 112, 0, 15.067, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(179, 179, 12, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(180, 180, 9, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(181, 181, 3, 18, 101, 0, 17.3512, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(182, 182, 6, 24, 123, 0, 15.5331, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(183, 183, 8, 24, 121, 0, 16.324, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(184, 184, 0, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(185, 185, 6, 19, 118, 0, 13.7891, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(186, 186, 4, 16, 100, 0, 16.4, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(187, 187, 9, 33, 127, 0, 20.46, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(188, 188, 7, 23, 119, 0, 16.3124, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(189, 189, 5, 19, 105, 0, 16.9615, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(190, 190, 11, 60, 147, 0, 27.6274, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(191, 191, 12, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(192, 192, 10, 29, 136, 0, 15.4087, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(193, 193, 5, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(194, 194, 10, 28, 142, 0, 13.9357, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(195, 195, 8, 22, 120, 0, 15.3472, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(196, 196, 6, 16, 109, 0, 13.7194, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(197, 197, 9, 28, 136, 0, 15.2465, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(198, 198, 11, 35, 139, 0, 18.1668, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(199, 199, 12, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(200, 200, 9, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(201, 201, 0, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(202, 202, 8, 25, 135, 0, 13.882, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(203, 203, 3, 14, 96, 0, 14.974, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(204, 204, 11, 38, 144, 0, 18.3738, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(205, 205, 7, 22, 123, 0, 14.2772, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(206, 206, 8, 28, 126, 0, 17.7627, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(207, 207, 6, 20, 115, 0, 14.9716, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(208, 208, 4, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(209, 209, 6, 20, 116, 0, 14.9376, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(210, 210, 11, 49, 157, 0, 19.798, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(211, 211, 8, 32, 132, 0, 18.2507, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(212, 212, 2, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE'),
(213, 213, 11, 0, 0, 0, 0, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(214, 214, 1, 9, 78, 0, 14.4642, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'MUJER'),
(215, 215, 8, 31, 131, 0, 18.2973, 'bajopeso', 'bajopeso', 'Bimestre1--2016', 'HOMBRE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Niños`
--

DROP TABLE IF EXISTS `Niños`;
CREATE TABLE IF NOT EXISTS `Niños` (
  `id` int(4) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_bin NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_bin NOT NULL,
  `sexo` varchar(255) COLLATE utf8_bin NOT NULL,
  `fechaNacimiento` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `Niños`
--

INSERT INTO `Niños` (`id`, `nombre`, `apellidos`, `sexo`, `fechaNacimiento`) VALUES
(1, 'LIZBETH', 'AGUILAR LOPEZ', 'MUJER', '2004'),
(2, 'KEVIN TADEO', 'ALBARRAN  GARCIA', 'HOMBRE', '2011'),
(3, 'EVA GUADALUPE', 'ALBARRAN MENDEZ', 'MUJER', '2012'),
(4, 'YESENIA JOSELIN', 'ALBARRAN MENDEZ', 'MUJER', '2018'),
(5, 'LIDIA', 'ALBEDA GRANADOS', 'MUJER', '2018'),
(6, 'DIEGO', 'ALDANA MARTINEZ', 'HOMBRE', '2013'),
(7, 'MARIA GUADALUPE', 'ALONSO SILVERIO', 'MUJER', '2006'),
(8, 'MAYRA', 'ALONSO SILVERIO', 'MUJER', '2004'),
(9, 'MARIA GUADALUPE', 'ANTONIO MAGDALENO', 'MUJER', '2015'),
(10, 'VERONICA ALEXANDRA', 'ANTONIO  MAGDALENO', 'MUJER', '2005'),
(11, 'OBET TENDARONI', 'ARRIOJA  ESCUTIA', 'MUJER', '2013'),
(12, 'ISAAC MISAEL', 'ARRIOJA ESCUTIA', 'HOMBRE', '2007'),
(13, 'ZILA VALENTINA', 'ARRIOJA  ESCUTIA', 'MUJER', '2010'),
(14, 'ZURI SADAI', 'ARRIOJA  ESCUTIA', 'MUJER', '2012'),
(15, 'ERIKA BETZABET', 'AVILA MENDOZA', 'MUJER', '2013'),
(16, 'KARLA YALANDIN', 'AVILA MENDOZA', 'MUJER', '2013'),
(17, 'KEVIN DAIR', 'AVILA MENDOZA', 'HOMBRE', '2007'),
(18, 'YEMILI', 'AVILA MENDOZA', 'MUJER', '2009'),
(19, 'HUGO JESUS', 'BALLADO  RAMIREZ', 'HOMBRE', '2005'),
(20, 'FATIMA VIANEY', 'BANDA MARTINEZ', 'MUJER', '2012'),
(21, 'RODRIGO DE JESUS', 'BAUTISTA  ARIZMENDI', 'HOMBRE', '2006'),
(22, 'ZAIRA', 'BAUTISTA  ARIZMENDI', 'MUJER', '2003'),
(23, 'ADRIAN', 'BAUTISTA  NAVARRO', 'HOMBRE', '2006'),
(24, 'DIEGO', 'BILLALBA BAUTISTA', 'HOMBRE', '2012'),
(25, 'DANIEL', 'BUENDIA VAZQUEZ', 'HOMBRE', '2003'),
(26, 'KAREN DANIELA', 'BUENDIA VAZQUEZ', 'MUJER', '2004'),
(27, 'EMILI VALERIA', 'CADENA RUEDA', 'MUJER', '2018'),
(28, 'SHARON', 'CAMACHO  BANDA', 'MUJER', '2013'),
(29, 'KAIRON JOSSEFT', 'CHAVEZ SANDOVAL', 'HOMBRE', '2010'),
(30, 'HECTOR JESUS', 'CHAVEZ SOLANO', 'HOMBRE', '2003'),
(31, 'JOSELIN MARLEN', 'CHAVEZ SOLANO', 'MUJER', '2005'),
(32, 'OSCAR ERNESTO', 'CHAVEZ SOLANO', 'HOMBRE', '2008'),
(33, 'RODOLFO ANGEL', 'CHAVEZ SOLANO', 'HOMBRE', '2010'),
(34, 'EDWIN JHOAN', 'CLARA  SANDOVAL', 'HOMBRE', '2006'),
(35, 'JIMENA', 'COLIN  MIRANDA', 'MUJER', '2003'),
(36, 'MELANI', 'COLIN  MIRANDA', 'MUJER', '2008'),
(37, 'TANIA ITZEL', 'COLIN  MIRANDA', 'MUJER', '2006'),
(38, 'CRISTIAN', 'CORTES MARTINEZ', 'HOMBRE', '2005'),
(39, 'IRENE LOURDES', 'CORTES MARTINEZ', 'MUJER', '2010'),
(40, 'YOSELIN', 'CORTES MARTINEZ', 'MUJER', '2018'),
(41, 'EMMANUEL', 'CRUZ  INFANTE', 'HOMBRE', '2015'),
(42, 'OMAR', 'CRUZ  INFANTE', 'HOMBRE', '2015'),
(43, 'YEIMI YADIRA', 'DIAZ  DOMINGUEZ', 'MUJER', '2006'),
(44, 'JULIAN', 'DIAZ  REYES', 'HOMBRE', '2007'),
(45, 'JULIANA ITZEL', 'DIAZ  REYES', 'MUJER', '2004'),
(46, 'EDGAR', 'DOMINGUEZ VILLALBA', 'HOMBRE', '2015'),
(47, 'DARLENE', 'DOMINGUEZ CAMPOS', 'MUJER', '2004'),
(48, 'PEDRO DAMIAN', 'DOMINGUEZ CAMPOS', 'HOMBRE', '2008'),
(49, 'CARLOS', 'ELIZARRARAS MOCTEZUMA', 'HOMBRE', '2015'),
(50, 'FRANCISCO', 'ESPINOSA TEMPLOS', 'HOMBRE', '2004'),
(51, 'PAOLA CITLALI', 'GARCIA DOMINGUEZ', 'MUJER', '2005'),
(52, 'ANGEL ISIDRO', 'GARCIA HERNANDEZ', 'HOMBRE', '2015'),
(53, 'PATRICIA RUBI', 'GARCIA HERNANDEZ', 'MUJER', '2015'),
(54, 'ALDAIR', 'GARCIA MATA', 'HOMBRE', '2004'),
(55, 'LUIS FRANCISCO', 'GARCIA REYES', 'HOMBRE', '2006'),
(56, 'OLIVER NICOLAS', 'GARCIA REYES', 'HOMBRE', '2012'),
(57, 'PERLA ESMERALDA', 'GARCIA REYES', 'MUJER', '2004'),
(58, 'AIXA MARIBEL', 'GARNICA HERNANDEZ', 'MUJER', '2015'),
(59, 'ISMAEL', 'GARNICA HERNANDEZ', 'HOMBRE', '2015'),
(60, 'ISRAEL', 'GARNICA HERNANDEZ', 'HOMBRE', '2015'),
(61, 'CARLOS ANDRES', 'GASPAR CUETO', 'HOMBRE', '2005'),
(62, 'EDGAR', 'GASPAR CUETO', 'HOMBRE', '2009'),
(63, 'JEIMY CAMILA', 'GONZALEZ GASPAR', 'MUJER', '2018'),
(64, 'BARBARA ALONDRA', 'GONZALEZ MARTINEZ', 'MUJER', '2006'),
(65, 'JUAN ANGEL', 'GONZALEZ MARTINEZ', 'HOMBRE', '2011'),
(66, 'MARCO ANTONIO', 'HERNANDEZ ALEJO', 'HOMBRE', '2005'),
(67, 'MA. GUADALUPE', 'HERNANDEZ ALEJO', 'MUJER', '2003'),
(68, 'ANGEL GABRIEL', 'HERNANDEZ HERNANDEZ', 'HOMBRE', '2015'),
(69, 'LUIS FERNANDO', 'HERNANDEZ HERNANDEZ', 'HOMBRE', '2015'),
(70, 'CARLOS DANIEL', 'HERNANDEZ MARTINEZ', 'HOMBRE', '2008'),
(71, 'JAZMIN', 'HERNANDEZ MARTINEZ', 'MUJER', '2006'),
(72, 'DIANA KAREN', 'HERNANDEZ VELASCO', 'MUJER', '2004'),
(73, 'ESAU', 'HERNANDEZ VELASCO', 'HOMBRE', '2003'),
(74, 'MERITZEL NAOMI', 'HERNANDEZ ONTIVEROS', 'MUJER', '2018'),
(75, 'CARLOS DANIEL', 'HERNANDEZ MARTINEZ', 'HOMBRE', '2015'),
(76, 'AIDE', 'HERRERA ONTIVEROS', 'MUJER', '2012'),
(77, 'AIDEE ALEXANDRA', 'HERRERA  ONTIVEROS', 'MUJER', '2015'),
(78, 'JENIFER', 'JACINTO VELASCO', 'MUJER', '2018'),
(79, 'MIGUEL ANGEL', 'JACINTO VELASCO', 'HOMBRE', '2009'),
(80, 'DILAN ESAU', 'JIMENEZ ESCUTIA', 'HOMBRE', '2010'),
(81, 'ITZEL GUADALUPE', 'JIMENEZ JUAREZ', 'MUJER', '2008'),
(82, 'JENIFER JAZMIN', 'JIMENEZ JUAREZ', 'MUJER', '2010'),
(83, 'OSVALDO DANIEL', 'JIMENEZ JUAREZ', 'HOMBRE', '2004'),
(84, 'ANGELICA ANAHI', 'JIMENEZ LOPEZ', 'MUJER', '2010'),
(85, 'JOSE ENRIQUE', 'JIMENEZ LOPEZ', 'HOMBRE', '2011'),
(86, 'DANNA JARITZY', 'JUAREZ MARTINEZ', 'MUJER', '2010'),
(87, 'KEVIN', 'JUAREZ MARQUEZ', 'HOMBRE', '2011'),
(88, 'ELIAS MIGUEL', 'JURADO CELAYA', 'HOMBRE', '2005'),
(89, 'EMIR', 'LABRA CORTES', 'HOMBRE', '2007'),
(90, 'MAGALI', 'LAGUNA PERALTA', 'MUJER', '2015'),
(91, 'JENNIFER', 'LAGUNA PERALTA', 'MUJER', '2015'),
(92, 'MIGUEL', 'LAGUNA PERALTA', 'HOMBRE', '2015'),
(93, 'ANA', 'LAGUNA PERALTA', 'MUJER', '2015'),
(94, 'ANSELMO', 'LAGUNA PERALTA', 'HOMBRE', '2015'),
(95, 'KEVIN', 'LAGUNA PERALTA', 'HOMBRE', '2015'),
(96, 'ANGELA ASTRID', 'LOPEZ MARTINEZ', 'MUJER', '2002'),
(97, 'JUAN CARLOS', 'LOPEZ MARTINEZ', 'HOMBRE', '2003'),
(98, 'KENYA ELIZABETH', 'LOPEZ MARTINEZ', 'MUJER', '2007'),
(99, 'ANGEL ABEL', 'LOZANO MORA', 'HOMBRE', '2007'),
(100, 'PENIEL', 'LUIZ DE LOS SANTOS', 'HOMBRE', '2005'),
(101, 'ISAI', 'LUIZ  DE LOS SANTOS', 'HOMBRE', '2006'),
(102, 'ELI', 'LUIZ  DE LOS SANTOS', 'HOMBRE', '2011'),
(103, 'LESLY AYDEE', 'LUNA SANDOVAL', 'MUJER', '2004'),
(104, 'ARON ADRIAN', 'MACHORRO AVILA', 'HOMBRE', '2013'),
(105, 'NARENDA', 'MACHORRO AVILA', 'MUJER', '2005'),
(106, 'ALDO SEBASTIAN', 'MACIAS BANDA', 'HOMBRE', '2007'),
(107, 'VANIA JAQUELINE', 'MACIAS BANDA', 'MUJER', '2005'),
(108, 'GABRIEL', 'MANDUJANO MEJIA', 'HOMBRE', '2007'),
(109, 'RAYMUNDO', 'MANDUJANO MEJIA', 'HOMBRE', '2018'),
(110, 'ARTURO', 'MANDUJANO MEJIA', 'HOMBRE', '2018'),
(111, 'ARGELIA', 'MANDUJANO MEJIA', 'MUJER', '2011'),
(112, 'BRUNO', 'MANDUJANO MEJIA', 'HOMBRE', '2018'),
(113, 'DENISSE', 'MARTINEZ AYALA', 'MUJER', '2010'),
(114, 'DERECK', 'MARTINEZ ESCUTIA', 'HOMBRE', '2012'),
(115, 'JUAN ANGEL', 'MARTINEZ GONZALEZ', 'HOMBRE', '2015'),
(116, 'DULCE ESMERALDA', 'MARTINEZ HERNANDEZ', 'MUJER', '2004'),
(117, 'JESUS', 'MARTINEZ HERNANDEZ', 'HOMBRE', '2002'),
(118, 'ARMANDO', 'MATIAS CADENA', 'HOMBRE', '2015'),
(119, 'NILTON FERNANDO', 'MATIAS CADENA', 'HOMBRE', '2015'),
(120, 'ARLETTE IVONNE', 'MATIAS HERNANDEZ', 'MUJER', '2006'),
(121, 'JESSICA', 'MATIAS HERNANDEZ', 'MUJER', '2012'),
(122, 'KEVIN', 'MATIAS HERNANDEZ', 'HOMBRE', '2014'),
(123, 'EILLEEN', 'MEDINA MARRON', 'MUJER', '2018'),
(124, 'LUIS IGNACIO', 'MEDINA MARRON', 'HOMBRE', '2008'),
(125, 'ISAIAS', 'MEJIA ALBEDA', 'HOMBRE', '2007'),
(126, 'KARINA', 'MEJIA ALBEDA', 'MUJER', '2003'),
(127, 'ANDREA', 'MENDEZ BARROCO', 'MUJER', '2011'),
(128, 'MANUEL ANTONIO', 'MENDEZ BARROCO', 'HOMBRE', '2005'),
(129, 'BRAULIO SAMUEL', 'MORA GONZALEZ', 'HOMBRE', '2006'),
(130, 'ESTEBAN ISRAEL', 'MORALES ESTRADA', 'HOMBRE', '2015'),
(131, 'MIRELLA ELIZABETH', 'MORALES ESTRADA', 'MUJER', '2006'),
(132, 'SORAYA SARAI', 'MORALES ESTRADA', 'MUJER', '2003'),
(133, 'YESSICA JOSELIN', 'MORALES ESTRADA', 'MUJER', '2004'),
(134, 'MA. DE LOS ANGELES', 'MORALES MEJIA', 'MUJER', '2008'),
(135, 'PAOLA', 'MORALES VELASCO', 'MUJER', '2015'),
(136, 'ADOLFO JESUS', 'MORENO GARCIA', 'HOMBRE', '2007'),
(137, 'ANGEL DANIEL', 'MURILLO SANTIAGO', 'HOMBRE', '2004'),
(138, 'EDITH', 'MURILLO SANTIAGO', 'MUJER', '2009'),
(139, 'YOSELIN', 'MURILLO SANTIAGO', 'MUJER', '2005'),
(140, 'CRISTIAN JOSE', 'NAVEDA  MARRON', 'HOMBRE', '2018'),
(141, 'GPE. ADRIAN', 'NAVEDA NICOLAS', 'MUJER', '2007'),
(142, 'JOANA BRILLIT', 'NICOLAS MARIN', 'MUJER', '2015'),
(143, 'AKETZALI SARAHI', 'NICOLAS ESCUTIA', 'MUJER', '2008'),
(144, 'JOSHUA EMMANUEL', 'NICOLAS ESCUTIA', 'HOMBRE', '2009'),
(145, 'JOSE ANTONIO', 'ORTIZ  OSORIO', 'HOMBRE', '2009'),
(146, 'DIEGO ALFONSO', 'OSORIO HERNANDEZ', 'HOMBRE', '2015'),
(147, 'GABRIELA', 'OSORIO HERNANDEZ', 'MUJER', '2015'),
(148, 'SANDRA GPE.', 'OSORIO HERNANDEZ', 'MUJER', '2015'),
(149, 'DANIELA RUBI', 'PALACIOS MAYORAL', 'MUJER', '2007'),
(150, 'DIEGO', 'PALACIOS MAYORAL', 'HOMBRE', '2004'),
(151, 'ANA LAURA', 'PAZ MEZA', 'MUJER', '2003'),
(152, 'ANA YOSELIN', 'PAZ MEZA', 'MUJER', '2008'),
(153, 'ANA YURIDIA', 'PAZ MEZA', 'MUJER', '2005'),
(154, 'IRVIN ALEXIS', 'PAZ MEZA', 'HOMBRE', '2004'),
(155, 'DIANA', 'PELAEZ  ANGUIANO', 'MUJER', '2008'),
(156, 'ANTONIO', 'PE?A HERNANDEZ', 'HOMBRE', '2004'),
(157, 'ESMERALDA', 'PE?A HERNANDEZ', 'MUJER', '2007'),
(158, 'MARLENE', 'PE?A HERNANDEZ', 'MUJER', '2002'),
(159, 'KENYA MONSERRAT', 'PEREZ VARONA', 'MUJER', '2011'),
(160, 'LUIS ANGEL', 'QUIRINO MATIAS', 'HOMBRE', '2008'),
(161, 'ROSA MARIA', 'QUIRINO MATIAS', 'MUJER', '2005'),
(162, 'JESSICA', 'RAMIREZ ARANDA', 'MUJER', '2006'),
(163, 'YULIANA', 'RAMIREZ RAMON', 'MUJER', '2007'),
(164, 'JOSELIN', 'RAMIREZ VAZQUEZ', 'MUJER', '2015'),
(165, 'AMERICA LIZBETH', 'RAMIREZ VAZQUEZ', 'MUJER', '2015'),
(166, 'HIROMI SARAHI', 'REYES GASPAR', 'MUJER', '2007'),
(167, 'KATHERINE', 'REYES LOPEZ', 'MUJER', '2007'),
(168, 'ISABEL CAMILA', 'ROBLES MORALES', 'MUJER', '2011'),
(169, 'BRAYAN KEVIN', 'RODRIGUEZ MEJIA', 'HOMBRE', '2007'),
(170, 'RICARDO ARTURO', 'ROMERO HERNANDEZ', 'HOMBRE', '2018'),
(171, 'BRANDON', 'ROMERO MENDEZ', 'HOMBRE', '2008'),
(172, 'ANDREA', 'ROSALES MESA', 'MUJER', '2005'),
(173, 'ANGELA', 'ROSALES MESA', 'MUJER', '2007'),
(174, 'JOSE DAVID', 'ROSALES MESA', 'HOMBRE', '2003'),
(175, 'ERIK MISAEL', 'RUIZ  CUEVAS', 'HOMBRE', '2006'),
(176, 'LEBNY TAMARA', 'RUIZ ROMERO', 'MUJER', '2009'),
(177, 'MELANIE', 'RUIZ ROMERO', 'MUJER', '2007'),
(178, 'JOSHUA ZAMURI', 'SANCHEZ AGUILAR', 'HOMBRE', '2010'),
(179, 'CHRISTIAN IVAN', 'SANCHEZ LAGUNA', 'HOMBRE', '2003'),
(180, 'KARLA IVONNE', 'SANCHEZ LAGUNA', 'MUJER', '2006'),
(181, 'ANDREA JAZMIN', 'SANCHEZ ORTIZ', 'MUJER', '2018'),
(182, 'LUIS GABRIEL', 'SANCHEZ ORTIZ', 'HOMBRE', '2009'),
(183, 'CRISTIAN YAET', 'SANCHEZ SANTIAGO', 'HOMBRE', '2007'),
(184, 'EVANDER ANTONIO', 'SANCHEZ SANTIAGO', 'HOMBRE', '2015'),
(185, 'SHERLIN', 'SANDOVAL CASILLAS', 'MUJER', '2009'),
(186, 'CRISTIAN MICHAEL', 'SANTOS LOPEZ', 'HOMBRE', '2011'),
(187, 'CAROLINA ARLET', 'SOTELO OSORIO', 'MUJER', '2006'),
(188, 'EZEQUIEL', 'SOTELO OSORIO', 'HOMBRE', '2008'),
(189, 'LIZBETH', 'SOTELO OSORIO', 'MUJER', '2010'),
(190, 'LUCERO GPE.', 'SUAREZ GARCIA', 'MUJER', '2004'),
(191, 'DIEGO', 'TEJADA VARONA', 'HOMBRE', '2003'),
(192, 'HECTOR DE JESUS', 'TORRES CRUZ', 'HOMBRE', '2005'),
(193, 'IRVIN MICHEL', 'TORRES MEJIA', 'HOMBRE', '2018'),
(194, 'SAINY GERALDINE', 'TRUJILLO SANDOVAL', 'MUJER', '2005'),
(195, 'JOSE FERNANDO', 'VARGAS  CASTILLO', 'HOMBRE', '2007'),
(196, 'LUZ MARIA', 'VARGAS  CASTILLO', 'MUJER', '2009'),
(197, 'VANESSA YOSELIN', 'VARGAS  CASTILLO', 'MUJER', '2006'),
(198, 'JOSE LUIS', 'VARGAS CASTILLO', 'HOMBRE', '2004'),
(199, 'JAVIER', 'VAZQUEZ LEONARDO', 'HOMBRE', '2003'),
(200, 'MARTIN', 'VAZQUEZ LEONARDO', 'HOMBRE', '2006'),
(201, 'AXEL URIEL', 'VELEZ NICOLAS', 'HOMBRE', '2015'),
(202, 'ANTONIA', 'VIDAL MENDEZ ', 'MUJER', '2007'),
(203, 'JENIFER IRENE', 'VILLANUEVA RAMIREZ', 'MUJER', '2012'),
(204, 'JOSELIN PALOMA', 'VILLANUEVA RAMIREZ', 'MUJER', '2004'),
(205, 'MITZI ESTEFANIA', 'VILLANUEVA RAMIREZ', 'MUJER', '2008'),
(206, 'DAFNE YERALDINE', 'ZARAGOZA GONZALEZ', 'MUJER', '2007'),
(207, 'ZAHORI YULIANA', 'ZARAGOZA GONZALEZ', 'MUJER', '2009'),
(208, 'MEREDITH DANELY', 'MONROY  SANCHEZ', 'MUJER', '2011'),
(209, 'EMILI PAOLA', 'ONTIVEROS HERNANDEZ', 'MUJER', '2009'),
(210, 'ITAHI', 'DE LOS SANTOS RODRIGUEZ', 'MUJER', '2004'),
(211, 'ANDREA', 'DE LOS SANTOS RODRIGUEZ', 'MUJER', '2007'),
(212, 'HUGO', 'CRUZ  SAMPAYO', 'HOMBRE', '2013'),
(213, 'BELEN ARACELI', 'JAIMES AMARO', 'MUJER', '2004'),
(214, 'ASHLY MAGALI', 'ONTIVEROS HERNANDEZ', 'MUJER', '2018'),
(215, 'ANGEL ADRIAN', 'ROMERO  ONTIVEROS', 'HOMBRE', '2007');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Bimestres`
--
ALTER TABLE `Bimestres`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Medicion`
--
ALTER TABLE `Medicion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Niños`
--
ALTER TABLE `Niños`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Bimestres`
--
ALTER TABLE `Bimestres`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `Medicion`
--
ALTER TABLE `Medicion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=216;
--
-- AUTO_INCREMENT de la tabla `Niños`
--
ALTER TABLE `Niños`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=216;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
