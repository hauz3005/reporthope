
(function(){
    var t;
    function size(animate){
        if (animate == undefined){
            animate = false;
        }
        clearTimeout(t);
        t = setTimeout(function(){
            $("canvas").each(function(i,el){
                $(el).attr({
                    "width":$(el).parent().width(),
                    "height":$(el).parent().outerHeight()
                });
            });
            redraw(animate);
            var m = 0;
            $(".chartJS").height("");
            $(".chartJS").each(function(i,el){ m = Math.max(m,$(el).height()); });
            $(".chartJS").height(m);
        }, 30);
    }
    $(window).on('resize', function(){ size(false); });


    function redraw(animation){
        var options = {};
        if (!animation){
            options.animation = false;
        } else {
            options.animation = true;
        }


        var myLine = new Chart(document.getElementById("AsistenciaData").getContext("2d")).Bar(AsistenciaData);

        var myLine = new Chart(document.getElementById("IMC-hombres-menores").getContext("2d")).Bar(IMChombresmenores);
        var myLine = new Chart(document.getElementById("IMC-mujeres-menores").getContext("2d")).Bar(IMCmujeresmenores);

        var myLine = new Chart(document.getElementById("IMC-hombres").getContext("2d")).Bar(IMChombres);
        var myLine = new Chart(document.getElementById("IMC-mujeres").getContext("2d")).Bar(IMCmujeres);

        var myLine = new Chart(document.getElementById("TP-hombres-menores").getContext("2d")).Bar(TPhombresmenores);
        var myLine = new Chart(document.getElementById("TP-mujeres-menores").getContext("2d")).Bar(TPmujeresmenores);

        var myLine = new Chart(document.getElementById("TP-hombres").getContext("2d")).Bar(TPhombres);
        var myLine = new Chart(document.getElementById("TP-mujeres").getContext("2d")).Bar(TPmujeres);
        
    }


    size(true);

   $('.showSection').click(function() {
      var show = $(this).attr('data-show');
      $('.showWrapper').removeClass('showWrapper');
      if(show == "Asistencia"){
          $('.Asistencia').addClass('showWrapper');

      }
      else if(show == "IMC"){
          $('.IMC').addClass('showWrapper');
      }
      else if(show == "TallaPeso"){
          $('.TallaPeso').addClass('showWrapper');
      }
      size(true);

    });

}());
